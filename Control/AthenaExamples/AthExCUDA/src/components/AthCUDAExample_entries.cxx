//
// Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
//

// Local include(s).
#include "../LinearTransformStandaloneExampleAlg.h"
#include "../LinearTransformTaskExampleAlg.h"
#include "../LinearTransformAsyncExampleAlg.h"
#include "../TrackParticleCalibratorExampleAlg.h"

// Declare the "components".
DECLARE_COMPONENT( AthCUDAExamples::LinearTransformStandaloneExampleAlg )
DECLARE_COMPONENT( AthCUDAExamples::LinearTransformTaskExampleAlg )
DECLARE_COMPONENT( AthCUDAExamples::LinearTransformAsyncExampleAlg )
DECLARE_COMPONENT( AthCUDAExamples::TrackParticleCalibratorExampleAlg )
