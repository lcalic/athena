/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODMUONPREPDATA_MDTDRIFTCIRCLEAUXCONTAINER_H
#define XAODMUONPREPDATA_MDTDRIFTCIRCLEAUXCONTAINER_H

#include "xAODMuonPrepData/MdtDriftCircleFwd.h"
#include "xAODMuonPrepData/versions/MdtDriftCircleAuxContainer_v1.h"

// Set up a CLID for the class:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF(xAOD::MdtDriftCircleAuxContainer, 1108747397, 1)
#endif  // XAODMUONRDO_NRPCRDO_H
